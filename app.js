/**
 * Module dependencies
*/
var express  = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var http = require('http');
var url = require("url");
var path = path = require('path');
var uuid = require('node-uuid');

var vhost = 'nodejsapp.local'
var port     = process.env.PORT || 3000;
var ip     = process.env.IP || "localhost";

var app = express();

var connectionObject = require('./app/config/database')(mongoose);
var models = require('./app/models/models')(connectionObject.connection, connectionObject.autoIncrement);
require('./app/config/passport')(passport,models); // pass passport for configuration
/*var multer = require('multer');*/

/*var s3 = new AWS.S3();*/


app.configure(function() {
    // set up our express application
    app.set('port', port);
    app.use(express.logger('dev')); // log every request to the console
    app.use(express.cookieParser()); // read cookies (needed for auth)
    app.use(express.bodyParser()); // get information from html forms
    app.set('view engine', 'html'); // set up html for templating
    app.engine('.html', require('ejs').__express);
    app.set('views', __dirname + '/public');
    app.use(express.static(path.join(__dirname, 'public')));
    //app.use(express.session({ secret: 'keyboard cat' }));// persistent login sessions
    app.use(express.methodOverride());
    app.use(express.json());
    app.use(express.urlencoded());
    //app.use(flash()); // use connect-flash for flash messages stored in session

    //passport configuration
    app.use(passport.initialize());
    //app.use(passport.session());// persistent login sessions
    //provagg
    app.use(app.router); //init routing

});

require('./app/routes.js')(app, passport,models); // load our routes and pass in our app and fully configured passport

// development only
if (app.get('env') === 'development') {
    app.use(express.errorHandler());
};

// production only
if (app.get('env') === 'production') {
    // TODO
};

//express.vhost(vhost, app);
app.utility = {};
app.utility.workflow = require('./app/utils/workflow');
app.utility.sendmail = require('./app/utils/sendmail');
var server = http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + vhost+":"+server.address().port);
});


/*app.use(multer({ // https://github.com/expressjs/multer
  dest: './public/uploads/', 
  limits : { fileSize:100000 },
  rename: function (fieldname, filename) {
    return filename.replace(/\W+/g, '-').toLowerCase();
  },
  onFileUploadData: function (file, data, req, res) {
    // file : { fieldname, originalname, name, encoding, mimetype, path, extension, size, truncated, buffer }
    var params = {
      Bucket: 'makersquest',
      Key: file.name,
      Body: data
    };

    s3.putObject(params, function (perr, pres) {
      if (perr) {
        console.log("Error uploading data: ", perr);
      } else {
        console.log("Successfully uploaded data to myBucket/myKey");
      }
    });
  }
}));*/
/*var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'tabrez.akh@gmail.com',
        pass: 'chemistry786'
    }
});
transporter.sendMail({
    from: 'tabrez.akh@gmail.com',
    to: 'tabrezakhlaque@gmail.com',
    subject: 'test',
    text: 'test!'
});*/
