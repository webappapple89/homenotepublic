module.exports = function(models){

    var TeacherDetails = models.teacherDetails;
    var Profile        = models.profile;
    var User           = models.user;
    var Session        = models.session;
    var SessionDetails = models.sessionDetails;
    url = require("url");
    var helper = {
        commonutil : require('./../utils/commonutil.js')
    };
    var moment = {
        moment : require('./../utils/moment/moment.js')
    };
    
 
   
    return {
        addSessionDetails : function (req, res){
            
            console.log (req.body);

            var session = req.body;


            User.findOne
             (
             	{ 'user_id' : req.body.user_id }, 
             	function (err, user) 
             	{
                	if (err || (!user))
                	{
                    	  res.send(500,helper.commonutil.failure(err));
                	}
                	if (user) 
                	{
                		if (session.record_id != undefined)
                		{
            				Session.update
            				(
            					{user_id : session.user_id, record_id : session.record_id},
            					{active : false},
            					function (error, sessionObjectFromDB)
            					{
            						if (error || (!sessionObjectFromDB))
            						{
            							console.log	("session updated");
            						}
            					}
            				);

            				SessionDetails.update
            				(
            					{user_id : session.user_id, record_id : session.record_id},
            					{active : false},
            					function (error, sessionObjectFromDB)
            					{
            						if (error || (!sessionObjectFromDB))
            						{
            							console.log	("session updated");
            						}
            					}
            				);
            			}
            			var record_id = moment.moment ().valueOf();

            			var start_date = moment.moment (session.start_date);
            			var end_date = moment.moment (session.end_date);


            			//TODO : 
            			//1. Check all dates and times are valid 
            			//2. Start date greater than current time
            			//3. End date greater than start date
            			//4. Will store all times into UTC. At client side needs to be converted as per user time stamp

                        /*console.log (session.instrument);
                        session.instrument[0] = 6;
                        session.instrument[1] = 7;*/


            			var recordAdded = false;
            			while (start_date <= end_date)
            			{
            				var day = moment.moment.weekdaysShort (start_date.weekday ());

            				if ( ((day == "Mon") && (session.monday))    ||
            					 ((day == "Tue") && (session.tuesday))   ||
            					 ((day == "Wed") && (session.wednesday)) ||
            					 ((day == "Thu") && (session.thursday))  ||
            					 ((day == "Fri") && (session.friday))    ||
            					 ((day == "Sat") && (session.saturdary)) ||
            					 ((day == "Sun") && (session.sunday)))
            				{
            					recordAdded = true;
	            				var sessionDetailsObject = new SessionDetails({ 
	            							user_id     : session.user_id,
									        record_id   : record_id,
									        title       : session.title,
                                            instrument  : session.instrument,
                                            skill_level : session.skill_level,
									        date 	    : start_date,
									        start_time  : session.start_time,
									        end_time    : session.end_time,
									        active      : true });

		                        sessionDetailsObject.save(function (err, sessionDetailsRecordFromDB) 
		            			{
		                        	if (err)
		                        	{
		                        		res.send(500,helper.commonutil.failure(err));
		                        		return;
		                        	}

		                    	});
	                    	}
            				start_date.add(1, 'day');

            			}

            			if (recordAdded == false)
            			{
            				res.send(500,helper.commonutil.failure("No record Added"));
            				return;
            			}
            			var sessionObject = new Session({ 
            							user_id     : session.user_id,
								        record_id   : record_id,
								        title       : session.title,
                                        instrument  : session.instrument,
                                        skill_level : session.skill_level,
								        start_date  : moment.moment (session.start_date) ,
								        end_date    : moment.moment (session.end_date),
								        start_time  : session.start_time,
								        end_time    : session.end_time,
								        monday      : session.monday,
								        tuesday     : session.tuesday,
								        wednesday   : session.wednesday,
								        thursday    : session.thursday,
								        friday      : session.friday,
								        saturdary   : session.saturdary,
								        sunday      : session.sunday,
								        active      : true });
            			sessionObject.save(function (err, sessionRecordFromDB) 
            			{
                        	if (err)
                        	{
                        		res.send(500,helper.commonutil.failure(err));
                        		return;
                        	}
                        	res.json(helper.commonutil.success('Added Session Event Successfully!'));
                    	});
  			
                	}
                }
			);               
              
        },
        getSessionDetails : function(req, res)
        {

        	parsedUrl = url.parse(req.url, true); // true to get query as object
         	
         	var queryAsObject = parsedUrl.query;
         
           	var user_id = queryAsObject.id;

            User.findOne({ 'user_id' : user_id }, function (err, user) 
            {
                if (err || (!user))
                {
                      res.send(500,helper.commonutil.failure(err));
                }
                if (user) 
                {
                    SessionDetails.find({ 'user_id' : user_id, active : true }, function (err, sessionDetailsRecordsFromDB) 
		            {
		                if (err || (!sessionDetailsRecordsFromDB))
		                {
		                      res.send(500,helper.commonutil.failure(err));
		                }
		                if (sessionDetailsRecordsFromDB) 
		                {
							res.json(sessionDetailsRecordsFromDB);
                        	res.end();
				               
		                }
		            });
                }
            });

        },
        getAllTeacherSessions : function(req, res)
        {

            var filter = req.body;

            var responseObj = [];

            console.log (filter);

            var query = {};

            if (filter.skill_level !== undefined)
            {
                query.skill_level = filter.skill_level;
            }

            if (filter.instrument !== undefined)
            {
                query.instrument = filter.instrument;
            }

            if (filter.date !== undefined)
            {
                var date = moment.moment (filter.date);

                //new Date('2012-05-16T20:54:35.630Z') } });

                var day = ("0" + date.get('date')).slice(-2);
                var month = ("0" + (date.get('month') + 1)).slice(-2);
                var dateString = date.get('year') + "-" + month  + "-" + day + "T00:00:00.000Z"; 
                query.start_date = { $gte : new Date (dateString)}; 
            }

            if ((filter.monday !== undefined) && (filter.monday == true))
            {
                query.monday = true;
            }


            if ((filter.tuesday !== undefined) && (filter.tuesday == true))
            {
                query.tuesday = true;
            }

            if ((filter.wednesday !== undefined) && (filter.wednesday == true))
            {
                query.wednesday = true;
            }

            if ((filter.thursday !== undefined) && (filter.thursday == true))
            {
                query.thursday = true;
            }

            if ((filter.friday !== undefined) && (filter.friday == true))
            {
                query.friday = true;
            }

            if ((filter.saturday !== undefined) && (filter.saturday == true))
            {
                query.saturday = true;
            }

            if ((filter.sunday !== undefined) && (filter.sunday == true))
            {
                query.sunday = true;
            }

            if (filter.start_time !== undefined)
            {
                query.start_time =  { $gte : filter.start_time} ;
            }

            if (filter.end_time !== undefined)
            {
                query.end_time =  { $lte : filter.end_time} ;
            }

            console.log (query);
            
            Session.distinct ( "user_id", query, function (err, obj)   
            {   
                if (err || (!obj))
                {

                }

                if (obj)
                {
                    //console.log (size);
                    User.find ( {"user_id" : { $in : obj} }, function (teacherErr, userObj)   
                    {

                        var teacherLength = userObj.length;
                        var teacherIndex = 0;
                        for (var count = 0; count < userObj.length; ++count)
                        {
                            var teacherQuery = {};

                            teacherQuery.user_id = userObj[count]._doc.user_id;

                            if (filter.min_price !== undefined)
                            {
                                teacherQuery.rate = { $gte : req.body.min_price};
                            }

                            if (filter.max_price !== undefined)
                            {
                                teacherQuery.rate = { $lte : req.body.max_price};
                            }

                            console.log ("Teacher query ");
                            console.log (teacherQuery);
                           
                             TeacherDetails.find ( teacherQuery, function (teacherErr, teacherObj)   
                            {
                                console.log (teacherObj);
                                if (teacherErr || (!teacherObj))
                                {
                                    console.log (teacherErr);
                                    teacherIndex ++;

                                }

                                if  ( (teacherObj[0] !== undefined) && (teacherObj[0]._doc.user_id !== undefined))
                                {

                                    //var instrument = userObj[teacherIndex]._doc.instrument;
                                    userObj[teacherIndex]._doc = helper.commonutil.merge_options ( userObj[teacherIndex]._doc, teacherObj[0]._doc);

                                    responseObj.push (userObj[teacherIndex]);
                                    //userObj[teacherIndex]._doc.instrument = instrument;
                                    //userObj[teacherIndex] += teacherObj
                                    console.log (userObj[teacherIndex]);
                                
                                }
                                else
                                {

                                }
                                teacherIndex ++;
                                if (teacherIndex == teacherLength)
                                {
                                    res.json(responseObj);

                                    res.end();
                                }
                            })
                        }

                    })
                   

                }
            });

        },
    }
}