module.exports = function(models){

    var TeacherDetails = models.teacherDetails;
    var Profile        = models.profile;
    var User           = models.user;
    url = require("url"),merge = require('merge');
    var helper = {
        commonutil : require('./../utils/commonutil.js')
    };
    
 
   
    return {
        addTeacherDetails : function (req, res){
            
            
            //console.log (teacherDetails);

             User.findOne
             (
             	{ 'user_id' : req.body.user_id }, 
             	function (err, user) 
             	{
                	if (err || (!user))
                	{
                    	  res.send(500,helper.commonutil.failure(err));
                	}
                	if (user) 
                	{

                		Profile.findOneAndUpdate 
                        (
			            	{user_id:req.body.user_id},
			            	{user_id:req.body.user_id, phone_number:req.body.phone_number, address : req.body.address, city : req.body.city,
			            	 state : req.body.state, country_iso3 : req.body.country_iso3, post_code : req.body.post_code,video:{video_url:req.body.video.video_url,video_type:req.body.video.video_type}},
			            	{new:true, upsert:true},
			            	function(err, profile) 
			            	{

			            		if (err || (!profile))
			            		{
			            			res.send(500,helper.commonutil.failure(err));
			            		}

			            		if (profile)
			            		{
			            			TeacherDetails.findOneAndUpdate 
			                        (
						            	{user_id:req.body.user_id},
						            	{user_id:req.body.user_id, instrument:req.body.instrument, languageSpeak : req.body.languageSpeak, rate : req.body.rate,
						            		qualification : req.body.qualification,  about : req.body.about,
						            		teachingstyle : req.body.teachingstyle,anythingelse : req.body.anythingelse, 
						            		isTrial: req.body.isTrial,video:{video_url:req.body.video.video_url,video_type:req.body.video.video_type}},
						            	{new:true, upsert:true},
						            	function(err, doc) 
						            	{

						            		if (err || (!doc))
						            		{
						            			res.send(500,helper.commonutil.failure(err));
						            		}

						            		if (doc)
						            		{
						            			res.json(doc);
			                        			res.end();
						            		}
						            		
						            	}
						            );
			            		}
			            		
			            	}
			            );
                    }
            	}
            );


           
        },
        secondAddTeacher : function(req,res){
        	
        	 User.findOne
             (
             	{ 'user_id' : req.body.user_id }, 
             	function (err, user) 
             	{
                	if (err || (!user))
                	{
                    	  res.send(500,helper.commonutil.failure(err));
                	}
                	if (user) 
                	{

                		Profile.findOne
                        (
			            	{user_id:req.body.user_id},
			            	function(err, profile) 
			            	{

			            		if (err || (!profile))
			            		{
			            			res.send(500,helper.commonutil.failure(err));
			            		}

			            		if (profile)
			            		{
			            			TeacherDetails.findOneAndUpdate 
			                        (
						            	{user_id:req.body.user_id},
						            	{user_id:req.body.user_id, instrument:req.body.instrument, languageSpeak : req.body.languageSpeak, rate : req.body.rate,
						            		qualification : req.body.qualification,  about : req.body.about,
						            		teachingstyle : req.body.teachingstyle,anythingelse : req.body.anythingelse, 
						            		isTrial: req.body.isTrial},
						            	{new:true, upsert:true},
						            	function(err, doc) 
						            	{

						            		if (err || (!doc))
						            		{
						            			res.send(500,helper.commonutil.failure(err));
						            		}

						            		if (doc)
						            		{
						            			res.json(doc);
			                        			res.end();
						            		}
						            		
						            	}
						            );
			            		}
			            		
			            	}
			            );
                    }
            	}
            );
        },
        thirdAddTeacher : function(req, res){
        	TeacherDetails.findOneAndUpdate 
			                        (	
			                        	{user_id:req.body.user_id},
						            	{video:{video_url:req.body.video.video_url,video_type:req.body.video.video_type}},
						            	{new:true, upsert:true},
						            	function(err, doc) 
						            	{

						            		if (err || (!doc))
						            		{
						            			res.send(500,helper.commonutil.failure(err));
						            		}

						            		if (doc)
						            		{
						            			res.json(doc);
			                        			res.end();
						            		}
						            		
						            	}
						            );
        },
        getTeacherDetails : function(req, res)
        {

        	parsedUrl = url.parse(req.url, true); // true to get query as object
         	
         	var queryAsObject = parsedUrl.query;
         
           	var user_id = queryAsObject.id;
           	var data = "";
            User.findOne({ 'user_id' : user_id }, function (err, user) 
            {
                if (err || (!user))
                {
                      res.send(500,helper.commonutil.failure(err));
                }
                if (user) 
                {
                	
                	data = user;
                	console.log(data);
                    Profile.findOne({ 'user_id' : user_id }, function (err, profile) 
		            {
		                if (err || (!profile))
		                {
		                	res.send(data);
		                	res.end();
		                      //res.send(500,helper.commonutil.failure(err));
		                }
		                if (profile) 
		                {
		                	
		                	console.log(profile);
		                	console.log(user);
		                	data= helper.commonutil.merge_options ( profile._doc, user._doc);
		                	//data = merge(data, profile);
		                	console.log(data);
		                    TeacherDetails.findOne({ 'user_id' : user_id }, function (err, teacherDetails) 
				            {
				                if (err || (!teacherDetails))
				                {
				                	
				                	res.send(data);
		                			res.end();
				                      //res.send(500,helper.commonutil.failure(err));
				                }
				                if (teacherDetails) 
				                {
				                	
				                	//data = merge(data, teacherDetails);
				                	console.log(data);	
				                	teacherDetails._doc = helper.commonutil.merge_options ( teacherDetails._doc, teacherDetails._doc.video);
				                	teacherDetails._doc = helper.commonutil.merge_options ( teacherDetails._doc, profile._doc);
				                	teacherDetails._doc = helper.commonutil.merge_options ( teacherDetails._doc, user._doc);
				             		res.json(teacherDetails._doc);
                        			res.end();
				                }
				            });
		                }
		            });
                }
            });

        },
    }
}