module.exports = function(models){

    var Profile = models.profile;
     var helper = {
        commonutil : require('./../utils/commonutil.js')
    }
    return {

       
        createProfile: function(req,res){

             //var profile = req.body.profile;
            var data = req.body.data;            
            var profile = new Profile({ 
            							user_id:1,
								        address: "",
								        city:"",
								        country:"String",
								        pin_code:"String",
								        phone:"String",
								        Social_Media_ID:"String",
								        createdDate:new Date(),
        								modifiedDate:new Date()});
            profile.save(function (err, user) {
                        if (err){
                        	res.send(500,helper.commonutil.failure(err));
                        }
                        res.json(helper.commonutil.success('Profile Created Successfully!'));
                    });
        }
    }
}