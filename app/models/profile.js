var mongoose = require('mongoose');


module.exports = function(connection, autoIncrement) {

    var Schema = mongoose.Schema;

    var profileSchema = new Schema({
    	user_id:Number,
        address: String,
        city:String,
        state:String,
        country_iso3:String,
        post_code:String,
        phone_number:String,
        Social_Media_ID:String,
        createdDate:String,
        modifiedDate:String
    },{collection: 'Profile'});


    var Profile = connection.model('Profile', profileSchema);

    return Profile;
}


