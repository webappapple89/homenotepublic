var mongoose = require('mongoose');
var moment = require('moment');

module.exports = function(connection, autoIncrement) {

        var Schema = mongoose.Schema;


        var sessionDetailsSchema = new Schema({
            user_id     : Number,
            record_id   : String,
            title       : String,
            instrument  : String,
            skill_level : String,
            date        : {type: Date, required: true},
            start_time  : String,
            end_time    : String,
            active      : Boolean,
            createDate: {type: Date, required: true, default: moment()}    
        },{collection: 'SessionDetails'});

        var SessionDetails = connection.model('SessionDetails', sessionDetailsSchema);

        return SessionDetails;

};
