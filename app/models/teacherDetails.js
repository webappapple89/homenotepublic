var mongoose = require('mongoose');


module.exports = function(connection, autoIncrement) {

    var Schema = mongoose.Schema;

    var teacherDetailsSchema = new Schema({
        user_id:Number,
        instrument:String,
        languageSpeak : String,
        rate : String,
        qualification : String,
        certification : Array,
        about	:  String,
        createdDate:String,
        modifiedDate:String,
        anythingelse:String,
        teachingstyle:String,
        isTrial:Boolean,
        video:{video_url:String,video_type:String}
    },{collection: 'Teacher_Details'});

    var TeacherDetails = connection.model('Teacher_Details', teacherDetailsSchema);

    return TeacherDetails;
}
