var mongoose = require('mongoose');


module.exports = function(connection) {

    var Schema = mongoose.Schema;

    var praticesetSchema = new Schema({
    	id:Number,
    	uniqueCode:String,
        publisherID: Number,
        name:String,
        description:String,
        subject:String,
        grade:String,
        accessMode:Number,
        attemptAllowed:Number,
        questionMode:Number,
        totalTime:Date,
        expiresOn:Date,
        notes:String,
        createdDate:Date,
        modifiedDate:Date
        
    },{ autoIndex: false });

    var PracticeSet = connection.model('PracticeSet', praticesetSchema);

    return PracticeSet;
}



