var mongoose = require('mongoose');
var moment = require('moment');
var expired_time = 60;

module.exports = function(connection, autoIncrement) {

    var Schema = mongoose.Schema;
    var userSchema = new Schema({
                                first_name: String,
                                last_name: String,
                                email: String,
                                pic_url: String,
                                username: String,
                                password: String,
                                is_active:Boolean,
                                profile_type:Number,
                                token:{
                                    auth_token: String,
                                    createDate: {type: Date, required: true, default: moment()}
                                },
                                activation:{
                                    password_reset_code: String,
                                    password_reset_time:Number,
                                    activation_code: String,
                                    validated:Boolean}
                            },{collection: 'User'});


    userSchema.methods.hasExpired = function() {
        return (moment().diff(this.token.createDate, 'minutes')) > expired_time;

    };
        userSchema.plugin(autoIncrement.plugin, { model: 'User',
                                                 field: 'user_id',
                                                    startAt: 1,
                                                    incrementBy: 1});
         var User = connection.model('User', userSchema);


    return User;
}

