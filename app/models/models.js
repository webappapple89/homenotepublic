module.exports = function(connection, autoIncrement) {

    var PracticeSet = require('./practiceset')(connection);
    var User = require('./user')(connection, autoIncrement);
    var Profile = require('./profile')(connection,autoIncrement);
    var TeacherDetails = require('./teacherDetails')(connection,autoIncrement);
    var Session = require('./session')(connection,autoIncrement);
    var SessionDetails = require('./sessionDetails')(connection,autoIncrement);

   

    return {
    	user: User,
        practiceset: PracticeSet,
        profile: Profile,
        teacherDetails   : TeacherDetails,
        session          : Session,
        sessionDetails   : SessionDetails
    }
}