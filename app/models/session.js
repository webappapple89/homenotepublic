var mongoose = require('mongoose');
var moment = require('moment');

module.exports = function(connection, autoIncrement) {

        var Schema = mongoose.Schema;

    var sessionSchema = new Schema({
        user_id     : Number,
        record_id   : String,
        title       : String,
        instrument  : String,
        skill_level : String,
        start_date  : Date,
        end_date    : Date,
        start_time  : String,
        end_time    : String,
        monday      : Boolean,
        tuesday     : Boolean,
        wednesday	: Boolean,
        thursday    : Boolean,
        friday      : Boolean,
        saturday    : Boolean,
        sunday      : Boolean,
        active      : Boolean,
        createDate  : {type: Date, required: true, default: moment()}
        
    },{collection: 'Session'});

    var Session = connection.model('Session', sessionSchema);

    return Session;

};