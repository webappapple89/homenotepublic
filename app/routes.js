module.exports = function(app, passport,models) {

    var api = {
        core :require('./api/api.js')(models),
        profileapi:require('./api/profileapi.js')(models),
        userapi:require('./api/userapi.js')(models),
        teacherapi:require('./api/teacherapi.js')(models),
        sessionapi:require('./api/sessionapi.js')(models),
    };
        


    app.get('/home', function(req, res){
        res.render('static/index',{});
    });
    app.get('/login', function(req, res){
        res.render('static/login',{});
    });
     app.get('/img', function(req, res){
        res.render('static/views/image',{});
    });
     app.get('/', function(req, res){
        res.render('static/indexold',{});
    });
     /* app.get('/wanttoteach', function(req, res){
        res.render('static/indexold',{});
    });*/
   /* app.post('/api/practiceset', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.teacherapi.createPracticeSet);*/

    app.post('/api/login', showClientRequest, passport.authenticate('local-login', {
        session: false
    }),api.core.login);
    /*Profile -Start*/

    app.get('/api/profile/createprofile',api.profileapi.createProfile);
     app.post('/api/user/register',api.userapi.registerUser);

     app.post('/api/user/initialRegister',api.userapi.initialRegister);
      app.post('/api/user/getUser',api.userapi.getUser);
      app.post('/api/user/authUser',api.userapi.getAuthUser);
      app.get('/api/user/uploadImage',api.userapi.uploadTest);
    app.get('/user/activate',api.userapi.activateUser);
     app.get('/api/user/getProfile',api.userapi.getProfile);
      app.get('/api/user/commonupload',api.userapi.commonUpload);

    /*Profile - End*/
    
    /*Teahcer Start*/
    app.get('/api/user/activateUser',api.userapi.activateUser);
    app.get('/api/user/getTeacherDetails',  api.teacherapi.getTeacherDetails);
    app.post('/api/user/teacherDetails',api.teacherapi.addTeacherDetails);
    app.post('/api/user/addTeacherSecond',api.teacherapi.secondAddTeacher);
     app.post('/api/user/addTeacherThird',api.teacherapi.thirdAddTeacher);
    /*Teacher End*/

    /*Session Start*/
    
    app.post('/api/session/sessionDetails',api.sessionapi.addSessionDetails);
    app.get('/api/session/getSessionDetails',  api.sessionapi.getSessionDetails);
    app.post('/api/session/getAllTeacherSessions',  api.sessionapi.getAllTeacherSessions);

    /*app.get('/partials/:name', showClientRequest, function (req, res) {
        var name = req.params.name;
        res.render('partials/' + name);
    });

    app.get('/partials/auth/:name', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),function (req, res) {
        var name = req.params.name;
        res.render('partials/auth/' + name);
    });

    app.post('/api/login', showClientRequest, passport.authenticate('local-login', {
        session: false
    }),api.login);

    app.post('/api/signup', showClientRequest, api.signup);


    app.get('/api/logout', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.logout);

    app.get('/api/people', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.getPeople);

    app.post('/api/person', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.createPerson);

    app.put('/api/person/:id', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.updatePerson);

    app.delete('/api/person/:id', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.removePerson);

    app.get('/api/things', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.getThings);

    app.post('/api/thing', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.createThing);

    app.put('/api/thing/:id', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.updateThing);

    app.delete('/api/thing/:id', showClientRequest, passport.authenticate('local-authorization', {
        session: false
    }),api.removeThing);*/











    function showClientRequest(req, res, next) {
        var request = {
            REQUEST : {
                HEADERS: req.headers,
                BODY : req.body
            }
        }
       // console.log(request)
        return next();
    }
}