module.exports = {

		success : function(msg){
			return {'success':true, 'msg': msg};
		},
		failure : function(msg){
			return {'success':false, 'msg': msg};
		},
		getDateTime : function (date) {
			    var hour = date.getHours();
			    hour = (hour < 10 ? "0" : "") + hour;

			    var min  = date.getMinutes();
			    min = (min < 10 ? "0" : "") + min;

			    var sec  = date.getSeconds();
			    sec = (sec < 10 ? "0" : "") + sec;

			    var year = date.getFullYear();

			    var month = date.getMonth() + 1;
			    month = (month < 10 ? "0" : "") + month;

			    var day  = date.getDate();
			    day = (day < 10 ? "0" : "") + day;

			    return year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

				},
		 merge_options  : function(obj1,obj2){
		    var obj3 = {};
		    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
		    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
		    return obj3;
		},
		concatJson: function(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}
};