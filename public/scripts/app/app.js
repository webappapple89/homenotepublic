/*
 * INSPINIA - Responsive Admin Theme
 * Copyright 2014 Webapplayers.com
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written stat for all view in theme.
 *
 */



    // app.js

var homenoteapp = angular.module('homenoteapp', ['ui.router','ui.bootstrap','LocalStorageModule','myAppServices', 'homenotedirectives','localytics.directives','ngSanitize','ui.calendar','chart.js']);
homenoteapp.controller('MainCtrl', function ($scope) {
    //Controller Code Here
});

homenoteapp.config(function($stateProvider, $urlRouterProvider) {
    
    //$urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'static/views/homenote/home.html'
        })
        .state('index', {
            url: '/',
            templateUrl: 'static/views/homenote/home.html'
        })
         .state('wanttoteach', {
            url: '/wanttoteach',
             controller:'RegistrationCtrl',
            templateUrl: 'static/views/homenote/register.html'
        })
		
		.state('addteacher1', {
            url: '/addteacher1/:teacherId',
            controller:'AddTeacherCtrl',
            templateUrl: 'static/views/homenote/add_teacher1.html'
        })
		.state('addteacher2', {
            url: '/addteacher2/:teacherId',
            controller:'AddTeacherCtrl',
            templateUrl: 'static/views/homenote/add_teacher2.html'
        })
		.state('addteacher3', {
            url: '/addteacher3/:teacherId',
            controller:'AddTeacherCtrl',
            templateUrl: 'static/views/homenote/add_teacher3.html'
        })
		.state('addteacher4', {
            url: '/addteacher4/:teacherId',
            controller:'AddTeacherCtrl',
            templateUrl: 'static/views/homenote/add_teacher4.html'
        })
		.state('publicityrealease', {
            url: '/publicity-realease',
            templateUrl: 'static/views/homenote/publicity-realease.html'
        })
		.state('privacypolicy', {
            url: '/privacy-policy',
            templateUrl: 'static/views/homenote/privacy-policy.html'
        })
		.state('termsconditions', {
            url: '/terms-conditions',
            templateUrl: 'static/views/homenote/terms-conditions.html'
        })
		
        .state('addTeacher', {
            url: '/addTeacher/:teacherId',
            controller:'AddTeacherCtrl',
            templateUrl: 'static/views/homenote/add_teacher.html'
        })
         .state('editProfile', {
            url: '/editProfile/:profileId',
            controller:'AddTeacherCtrl',
            templateUrl: 'static/views/homenote/add_teacher.html'
        })
        .state('teachercalendar', {
            url: '/teachercalendar/:teacherId',
            controller:'TeacherCalendarCtrl',
            templateUrl: 'static/views/homenote/teacher-calendar.html'
        })
         .state('loginPage', {
            url: "/loginPage",
            controller:'LoginCtrl',
            templateUrl: "static/views/homenote/login.html"
        })
          .state('login', {
            url: "/login",
            controller:'LoginCtrl',
            templateUrl: "static/views/homenote/login.html"
        })
        .state('register', {
            url: '/register',
            controller:'RegistrationCtrl',
            templateUrl: 'static/views/homenote/register.html'
        })
         .state('teacherprofile', {
            url: '/teacherprofile/:teacherId',
            controller:'TeacherProfileCtrl',
            templateUrl: 'static/views/homenote/teacher-profile.html'
        })
         .state('imageUpload', {
            url: '/imageUpload',
            templateUrl: 'static/views/homenote/image.html',
            controller:'ImgCtrl'
        })
         .state('registercomplete', {
            url: '/registercomplete',
            templateUrl: 'static/views/homenote/register_complete.html',
            controller:'RegistrationCtrl'
        })
          .state('beforeteach', {
            url: '/beforeteach',
            templateUrl: 'static/views/homenote/before_teach.html',
            controller:'RegistrationCtrl'
        })
           .state('howitworks', {
            url: '/howitworks',
            templateUrl: 'static/views/homenote/how_work.html',
            controller:'RegistrationCtrl'
        })
        .state('selectteacher', {
            url: '/selectteacher',
            templateUrl: 'static/views/homenote/select-teacher.html',
            controller:'SelectTeacherCtrl'
        })
         .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'static/views/homenote/teacher-dashboard.html',
            controller:'RegistrationCtrl'
        })

        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            // we'll get to this in a bit       
        });
        
});
homenoteapp.config(['$httpProvider',function ($httpProvider) {
        $httpProvider.interceptors.push('TokenInterceptor');
    }]);

homenoteapp.run(['$rootScope','$location','AuthenticationService',function($rootScope, $location, AuthenticationService) {
        $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {

            if (nextRoute.access===undefined) {
                $location.path("/loginPage");
            }else if (nextRoute.access.requiredLogin && !AuthenticationService.isLogged()) {
                $location.path("/loginPage");
            }else if (AuthenticationService.isLogged() && !nextRoute.access.requiredLogin) {
                $location.path("/home");
            }
        });
    }]);

$("#mySelect").chosen({
  "disable_search": true
});

$('.chosen-select').hide();

var loginapp = angular.module('loginapp', ['ui.router','ui.bootstrap','LocalStorageModule','myAppServices','homenotedirectives']);
