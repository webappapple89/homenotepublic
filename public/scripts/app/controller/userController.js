homenoteapp.controller('RegistrationCtrl', ['$scope', '$http','$location','$modal', 
        function ($scope, $http, $location, $modal) {
            $scope.formData = {isTnC:false};

            $scope.submit = function(isValid)
            {
                if(isValid == true){
                var salt = $scope.username;

               /* var enc_password = CryptoJS.PBKDF2($scope.password, salt, { keySize: 256/32 });
                var enc_check_password = CryptoJS.PBKDF2($scope.check_password, salt, { keySize: 256/32 });
*/
                var user = $scope.formData;
                var files = document.getElementById("profile_pic").files;
                var file = files[0];
                var filename = "profile."+file.name.split(".")[1];
                if(user.username!==undefined || user.password !==undefined || user.check_password !==undefined){

                    if(user.password !== user.check_password){
                        alert('password and check_password must be the same!');
                    }else{
                        $http({method:'Get',url:"/api/user/uploadImage?file_name="+filename+"&file_type="+file.type+"&username="+user.username+"&email="+user.email}).success(function(data, status, headers, config){
                            upload_file(file, data.signed_request, data.url);
                            user.pic_url = data.url;
                            $http({method: 'POST', url: '/api/user/register', data:user}).
                            success(function(data, status, headers, config) {
                             $location.path("/registercomplete"); 
                            }).
                            error(function(data, status, headers, config) {
                              homeNoteCommon.utils.showNotice('error',data);
                            });
                        }).
                        error(function(data, status, headers, config){
                                
                                noty({text: data.msg,  timeout: 2000, type: 'error'});
                            
                        });
                    }

                }else{
                   
                }
            } else {
                homeNoteCommon.utils.showNotice('danger','Please validate the form');
            }
            };

            $scope.firstRegistration = function(invalidForm){
                if(invalidForm == false){
                var user = $scope.formData;
                user.is_teacher = true;
                if(user.isTnC == false){
                    noty({text: "Please Accept Term and Conditions",  timeout: 3000, type: 'error'});
                    return;
                }
                $http({method: 'POST', url: '/api/user/initialRegister', data:user}).
                            success(function(data, status, headers, config) {
                             $location.path("/registercomplete"); 
                            }).
                            error(function(data, status, headers, config) {
                              homeNoteCommon.utils.showNotice('error',data);
                            });
                        } else {
                            noty({text: "Please provide all required field!!",  timeout: 2000, type: 'error'});
                        }
            }

             $scope.onTermandConditions = function()
    {

        var modalInstance = $modal.open({
            templateUrl: 'static/views/homenote/terms-conditions-popup.html',
            controller:TermsAndConditionController
            
        });

    }

        }
    ]);

homenoteapp.controller('ImgCtrl', ['$scope', '$http',
        function ($scope, $http) {
        }]);


homenoteapp.controller('MainAppCtrl', ['$scope', '$http','usersevice','localStorageService','AuthenticationService','$location','$sce',
        function ($scope, $http, usersevice, localStorageService,AuthenticationService, $location, $sce) {
            $scope.loggedInUser = {};
             $scope.labels = ['Session 1', 'Session 2', 'Session 3', 'Session 4', 'Session 5', 'Session 6', 'Session 7'];
  $scope.series = ['Enquired', 'Enrolled'];

  $scope.data = [
    [65, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90]
  ];
  $scope.redirectToDashboard = function(isActive) {
  if(isActive === true) {
    $location.path("/dashboard");
  }else {
  $location.path("/register");
  }
}
   $scope.ratingseries = ['Rating'];

  $scope.ratingdata = [
    [3, 5, 3, 4.5, 3, 4, 3.5]
  ];

  $scope.pielabels = ['Session 1', 'Session 2', 'Session 3', 'Session 4', 'Session 5', 'Session 6', 'Session 7'];
  $scope.piedata = [500, 700, 3000,700,600,1000,1700];
            if(localStorageService.get('auth_token')){
             $http({method: 'POST', url: '/api/user/authUser', data:{'auth_token':localStorageService.get('auth_token')}}).success(function(data){
                usersevice.setUserData(data);
                $scope.loggedInUser = data;
                $scope.loggedInUser.isActiveSession = true;
            }).error(function(){

            });
            }
            $scope.logout = function()
            {
                localStorageService.clearAll();
                window.location.href = "/home";
            }
             $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }
/*Pattern for validations*/
 $scope.REGEX_EMAIL = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/;
/*Pattern for validations*/        
$scope.isHideProfile = false;      

        }]);


homenoteapp.service('usersevice', function() {
    this.userData = {};
    this.setUserData = function(data){
        this.userData= data;
    }                 
    });


function upload_file(file, signed_request, url){
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", signed_request);
    xhr.setRequestHeader('x-amz-acl', 'public-read');
    xhr.onload = function() {
        if (xhr.status === 200) {           
            
        }
    };
    xhr.onerror = function() {
    };
    xhr.send(file);
}



homenoteapp.controller('UserCtrl', ['$scope', '$http','$location', '$stateParams', 
        function ($scope, $http,$location,$stateParams) {

    $scope.teacher = {};
    $scope.stateList = {};

    $scope.countryList     = homeNoteAppConfig.country;
    $scope.instrumentList  = homeNoteAppConfig.instrumentList;
    $scope.languageList    = homeNoteAppConfig.languageList;


     if($stateParams.profileId){
            $http({method: 'GET', url: '/api/user/getTeacherDetails?id='+$stateParams.profileId}).success(function(data){
                console.log(data);
                $scope.teacher = data;
            }).error(function(){

        });
    }
    $scope.onCountryChange = function ()
    {
        $scope.stateList = homeNoteAppConfig.state[$scope.teacher.country_iso3];
    }
    $scope.getObject = function (obj)
    {
        if ( ($scope.teacher.first_name !=  undefined ) &&
             ($scope.teacher.last_name  !=  undefined ) &&
             ($scope.teacher.email      !=  undefined ) )
        {
            obj.user_id       = $stateParams.teacherId;
            obj.phone_number  = $scope.teacher.phone_number;
            obj.address       = $scope.teacher.address;
            obj.city          = $scope.teacher.city;
            obj.state         = $scope.teacher.state;
            obj.country_iso3  = $scope.teacher.country_iso3;
            obj.post_code     = $scope.teacher.post_code;
            obj.instrument    = $scope.teacher.instrument;
            obj.languageSpeak = $scope.teacher.languageSpeak;
            obj.rate          = $scope.teacher.rate;
            obj.qualification = $scope.teacher.qualification;
            obj.certification = $scope.teacher.certification;
            
            return true;
        }
        return false;
        
    }
    $scope.addTeacher = function()
    {
        var teacherDetails={};
        if($scope.getObject(teacherDetails))
        {
            //var teacherDetails = {"name": $scope.teacher.name };
            $http({method: 'POST', url: '/api/user/teacherDetails', data:teacherDetails}).
            success(function(data, status, headers, config) {
                window.location.href= "/login";
                 
                     
                    }).
                    error(function(data, status, headers, config) 
                    {

                        
                        /*if(status===401)
                        {
                            noty({text: '',  timeout: 2000, type: 'error'});
                        }
                        else
                        {
                          noty({text: data,  timeout: 2000, type: 'error'});
                        }*/
                    });
        }
        else
        {
            noty({text: '!',  timeout: 2000, type: 'error'});
        }

    }

 
}

]);


function TermsAndConditionController($scope,$http,$location, $timeout,$modal,$modalInstance)
{
    $scope.acceptTerms = function(){
         document.getElementById('termandconditioncheck').checked=true;
         var scope = angular.element($("#termandconditioncheck")).scope();
        scope.formData.isTnC = true;
         $scope.close();
    }
   
    $scope.close = function () 
    {

        $modalInstance.dismiss('cancel');
    };
        
}