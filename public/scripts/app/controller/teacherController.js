

homenoteapp.service ('teacherservice', function() {

    this.teacherObj = {};


    this.setTeacherObj = function(obj) {
              this.teacherObj = obj;
      }
      this.resetTeacherObj = function() {
          this.teacherObj = {};
      }

      this.getTeacherObj = function() {
          return this.teacherObj;
      }

    /*return 
    {
      setTeacherObj : function(obj) {
              this.teacherObj = obj;
      },
      resetTeacherObj : function() {
          this.teacherObj = {};
      },
      getTeacherObj : function() {
          return this.teacherObj;
      },
    }*/

});

homenoteapp.controller('AddTeacherCtrl', ['$scope', '$http','$location', '$stateParams','$sce','localStorageService',
        function ($scope, $http,$location,$stateParams,$sce,localStorageService) {

    $scope.teacher = {certifications:[{}]};
    $scope.stateList = {};

    $scope.countryList     = homeNoteAppConfig.country;
    $scope.instrumentList  = homeNoteAppConfig.instrumentList;
    $scope.languageList    = homeNoteAppConfig.languageList;


    /* if($stateParams.teacherId){
            $http({method: 'POST', url: '/api/user/getUser', data:{'id':$stateParams.teacherId}}).success(function(data){
            	console.log(data);
            	$scope.teacher = data;
              $scope.formData = data;
                 $scope.teacher.certifications=[{}];
            }).error(function(){

        });
    }*/
    if($stateParams.teacherId)
     {
        $http({method: 'GET', url: '/api/user/getTeacherDetails?id='+$stateParams.teacherId}).success
        (
            function(data)
            {
                console.log(data);
                $scope.teacher = data;

                $scope.stateList = homeNoteAppConfig.state[$scope.teacher.country_iso3];
                $scope.formData = data;
                if(typeof $scope.formData.pic_url == 'undefined' || !$scope.formData.pic_url)
                {
                  $scope.formData.pic_url = "../static/img/profile_small1.jpg";
                }
                $scope.teacher.certifications=[{}];

            }
        ).error
        (
            function()
            {
            }
        );
    }
    $scope.onCountryChange = function ()
    {
        $scope.stateList = homeNoteAppConfig.state[$scope.teacher.country_iso3];
    }
    $scope.addCertificationRow = function(certification){
        homeNoteCommon.addQualificationRow(this.teacher.certifications, certification);
    }
     $scope.removeCertification = function(certification){
        homeNoteCommon.removeObject(this.teacher.certifications, certification);
    }
    $scope.getObject = function (obj)
    {
        if ( ($scope.teacher.first_name !=  undefined ) &&
             ($scope.teacher.last_name  !=  undefined ) &&
             ($scope.teacher.email      !=  undefined ) )
        {
            obj.user_id       = typeof $stateParams.teacherId == 'undefined' ? $stateParams.profileId : $stateParams.teacherId;
            obj.phone_number  = $scope.teacher.phone_number;
            obj.address       = $scope.teacher.address;
            obj.city          = $scope.teacher.city;
            obj.about          = $scope.teacher.about;
            obj.isTrial          = $scope.teacher.isTrial;
            obj.state         = $scope.teacher.state;
            obj.country_iso3  = $scope.teacher.country_iso3;
            obj.post_code     = $scope.teacher.post_code;
            obj.instrument    = $scope.teacher.instrument;
            obj.languageSpeak = $scope.teacher.languageSpeak;
            obj.rate          = $scope.teacher.rate;
            obj.qualification = $scope.teacher.qualification;
            obj.teachingstyle = $scope.teacher.teachingstyle;
             obj.anythingelse = $scope.teacher.anythingelse;
             obj.video = {"video_url":$scope.teacher.video_url,"video_type":$scope.teacher.video_type};
          //  obj.certification = $scope.teacher.certification;//Temproray comment to upload files
            
            return true;
        }
        return false;
        
    }
    $scope.resetTeacherForm = function(){
      $scope.formData = {};
      $scope.formData.pic_url = "../static/img/profile_small1.jpg";
    }
    $scope.submitFirstPageData = function(user){
      $http({method: 'POST', url: '/api/user/register', data:user}).
                            success(function(data, status, headers, config) {
                              if(data.success == true){
                             $location.path("/addteacher2/"+data.msg.user_id);
                             } 
                            }).
                            error(function(data, status, headers, config) {
                              homeNoteCommon.utils.showNotice('error',data);
                            });
    }
    $scope.submitTeacherData = function(postFixUrl){
      var nextPage = "addteacher3/";
      if(postFixUrl == "addTeacherThird"){
        nextPage = "addteacher4/";
      } 

       $http({method: 'POST', url: '/api/user/'+postFixUrl, data:$scope.teacher}).
                            success(function(data, status, headers, config) {
                                
                               if($stateParams.profileId)
                            { 
                              $location.path("teacherprofile/"+$stateParams.profileId); 
                          } else{
                            $location.path(nextPage+$scope.teacher.user_id); 
                            //$location.path("login"); 
                          }
                                    }).
                                    error(function(data, status, headers, config) 
                                    {
                                          noty({text: "Please validate the form !",  timeout: 2000, type: 'error'});
                                    });
    }
    $scope.firstAddTeacher = function(isValid){

                if(isValid == true){
                var salt = $scope.username;

               /* var enc_password = CryptoJS.PBKDF2($scope.password, salt, { keySize: 256/32 });
                var enc_check_password = CryptoJS.PBKDF2($scope.check_password, salt, { keySize: 256/32 });
*/
                var user = $scope.formData;
                var files = document.getElementById("profile_pic").files;
                var file = files[0];
               
                if(user.username!==undefined || user.password !==undefined || user.check_password !==undefined){

                    if(user.password !== user.check_password){
                       homeNoteCommon.utils.showNotice('danger','password and confirm password must be the same!');
                       
                    }else if(typeof file != 'undefined'){
                       var filename = "profile."+file.name.split(".")[1];
                        $http({method:'Get',url:"/api/user/uploadImage?file_name="+filename+"&file_type="+file.type+"&username="+user.username+"&email="+user.email+"&user_id="+user.user_id}).success(function(data, status, headers, config){
                            upload_file(file, data.signed_request, data.url);
                            user.pic_url = data.url;
                            $scope.submitFirstPageData(user);
                            /*$http({method: 'POST', url: '/api/user/register', data:user}).
                            success(function(data, status, headers, config) {
                              if(data.success == true){
                             $location.path("/addteacher2/"+data.msg.user_id);
                             } 
                            }).
                            error(function(data, status, headers, config) {
                              homeNoteCommon.utils.showNotice('error',data);
                            });*/
                        }).
                        error(function(data, status, headers, config){
                                
                                noty({text: data.msg,  timeout: 2000, type: 'error'});
                            
                        });
                    } else {
                       $scope.submitFirstPageData(user);
                    }

                }else{
                   
                }
              } else {
                homeNoteCommon.utils.enforceValidation();
                 noty({text: "Invalid Form , Kindly fill all data!!",  timeout: 2000, type: 'error'});
              }
           
    }

    /*  $scope.addTeacher = function()
    {
                
        var teacherDetails={};
      if($scope.getObject(teacherDetails))
      {
        //var teacherDetails = {"name": $scope.teacher.name };
            if(typeof document.getElementById("upload-Video") != 'undefined' && document.getElementById("upload-Video")){
              var files = document.getElementById("upload-Video").files;
                var file = files[0];
                var filename = typeof file != 'undefined'? "introvideo."+file.name.split(".")[1]: undefined;
             $http({method:'Get',url:"/api/user/commonupload?file_name="+filename+"&file_type="+file.type+"&username="+$scope.teacher.username+"&folder=video"}).success(function(data, status, headers, config){
                           teacherDetails.video = {"video_url":data.url,"video_type":file.type};
                            upload_file(file, data.signed_request, data.url);
                           $http({method: 'POST', url: '/api/user/teacherDetails', data:teacherDetails}).
                            success(function(data, status, headers, config) {
                               if($stateParams.profileId)
                            { 
                              $location.path("teacherprofile/"+$stateParams.profileId); 
                          } else{
                            $location.path("login"); 
                          }
                                    }).
                                    error(function(data, status, headers, config) 
                                    {
                                          noty({text: "Please validate the form !",  timeout: 2000, type: 'error'});
                                    });
                        }).
                        error(function(data, status, headers, config){
                                
                                noty({text: data.msg,  timeout: 2000, type: 'error'});
                            
                        });
                        }else {
                            $http({method: 'POST', url: '/api/user/addTeacherSecond', data:teacherDetails}).
                            success(function(data, status, headers, config) {
                                
                               if($stateParams.profileId)
                            { 
                              $location.path("teacherprofile/"+$stateParams.profileId); 
                          } else{
                            //$location.path("addteacher3/"+$stateParams.profileId); 
                            //$location.path("login"); 
                          }
                                    }).
                                    error(function(data, status, headers, config) 
                                    {
                                          noty({text: "Please validate the form !",  timeout: 2000, type: 'error'});
                                    });
                        }
            
        }
        else
        {
            noty({text: 'Error!',  timeout: 2000, type: 'error'});
        }

    }*/



      $scope.secondAddTeacher = function(isValid)
    {
         if(isValid == true){        
        var teacherDetails={};
      if($scope.teacher)
      {
        //var teacherDetails = {"name": $scope.teacher.name };
            
                             $scope.submitTeacherData("addTeacherSecond");
                        
            
        }
        else
        {
            noty({text: 'Error!',  timeout: 2000, type: 'error'});
        }
      }else {
        homeNoteCommon.utils.enforceValidation();
                 noty({text: "Invalid Form , Kindly fill all data!!",  timeout: 2000, type: 'error'});
      }
    }
    $scope.thirdAddTeacher = function(){
      var files = document.getElementById("upload-Video").files;
                var file = files[0];
                var filename = typeof file != 'undefined'? "introvideo."+file.name.split(".")[1]: undefined;
        var teacherDetails={};
      if($scope.getObject(teacherDetails))
      {
        //var teacherDetails = {"name": $scope.teacher.name };
            if(typeof filename != 'undefined'){
             $http({method:'Get',url:"/api/user/commonupload?file_name="+filename+"&file_type="+file.type+"&username="+$scope.teacher.username+"&folder=video"}).success(function(data, status, headers, config){
                           $scope.teacher.video = {"video_url":data.url,"video_type":file.type};
                            upload_file(file, data.signed_request, data.url);
                            $scope.submitTeacherData("addTeacherThird");
                        }).
                        error(function(data, status, headers, config){
                                
                                noty({text: data.msg,  timeout: 2000, type: 'error'});
                            
                        });
                        }else{
                          $scope.submitTeacherData("addTeacherThird");
                        }
    }
  }

     $scope.teacherPageOne = function()
    {
      $location.path("addteacher1/"+$scope.teacher.user_id);
  }
   $scope.teacherPageTwo = function()
    {
      $location.path("addteacher2/"+$scope.teacher.user_id);
  }
}

]);


homenoteapp.controller('TeacherProfileCtrl', ['$scope', '$http','$location', '$stateParams', 
        function ($scope, $http,$location,$stateParams) 
{

    $scope.teacher = {};
    $scope.stateList = {};

    $scope.countryList     = homeNoteAppConfig.country;
    $scope.instrumentList  = homeNoteAppConfig.instrumentList;
    $scope.languageList    = homeNoteAppConfig.languageList;


     if($stateParams.teacherId)
     {
        $http({method: 'GET', url: '/api/user/getTeacherDetails?id='+$stateParams.teacherId}).success
        (
            function(data)
            {
                $scope.teacher = data;
                $scope.teacher.instrument = data.instrument.split(",");
                $scope.teacher.languageSpeak = data.languageSpeak.split(",");

            }
        ).error
        (
            function()
            {
            }
        );
    }
}
]);


homenoteapp.controller('TeacherCalendarCtrl', ['$scope', '$http','$location', '$stateParams','$modal', 'uiCalendarConfig', 'teacherservice',
        function ($scope, $http,$location,$stateParams,$modal , uiCalendarConfig, teacherservice) 
{

    $scope.teacher = {};

    teacherservice.resetTeacherObj ();
    
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    $scope.changeTo = 'Hungarian';
    /* event source that pulls from google.com */
    $scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
    };
    /* event source that contains custom events on the scope 
    $scope.events = [
      {title: 'All Day Event',start: new Date(y, m, 1)},
      {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
      {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
      {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ];*/

    $scope.events = [];
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };

    $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: [ 
          {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
        ]
    };


     if($stateParams.teacherId)
     {
        teacherservice.setTeacherObj ($stateParams.teacherId);

        $http({method: 'GET', url: '/api/session/getSessionDetails?id='+$stateParams.teacherId}).success
        (
            function(data)
            {
                console.log(data);
                $scope.sessionDetails = data;
                //TODO  :Validation check pending
                //Also when we receive one object only code is pending

                for (var i = 0; i < $scope.sessionDetails.length ; ++i)
                {
                    var start_date_time = moment($scope.sessionDetails[i].date).hours($scope.sessionDetails[i].start_time.substring(0,2));
                    var end_date_time = moment($scope.sessionDetails[i].date).hours($scope.sessionDetails[i].end_time.substring(0,2));
                    $scope.events.push(
                    {title: $scope.sessionDetails[i].title ,start: start_date_time ,end: end_date_time });    
                }

            }
        ).error
        (
            function()
            {
            }
        );
    }

    

    /* alert on eventClick */
    $scope.alertOnEventClick = function( date, jsEvent, view){
        alert(date.title + ' was clicked ');
    };
    /* alert on Drop */
     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
       $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
      var canAdd = 0;
      angular.forEach(sources,function(value, key){
        if(sources[key] === source){
          sources.splice(key,1);
          canAdd = 1;
        }
      });
      if(canAdd === 0){
        sources.push(source);
      }
    };
    /* add custom event*/
    $scope.addEvent = function() {
      /*$scope.events.push(
      {title: 'Test',start: new Date(y, m, 29),end: new Date(y, 07, 30),url: 'http://test.com/'});*/
    };
    /* remove event */
    $scope.remove = function(index) {
      $scope.events.splice(index,1);
    };
    /* Change View */
    $scope.changeView = function(view,calendar) {
      uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
      if(uiCalendarConfig.calendars[calendar]){
        uiCalendarConfig.calendars[calendar].fullCalendar('render');
      }
    };
     /* Render Tooltip */
    $scope.eventRender = function( event, element, view ) { 
        element.attr({'tooltip': event.title,
                     'tooltip-append-to-body': true});
        //$compile(element)($scope);
    };

    $scope.onCreateSession = function()
    {

        var modalInstance = $modal.open({
            templateUrl: 'static/views/homenote/create_session.html' ,
            controller: CreateSessionCtrl
            
        });

    }

    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 450,
        editable: true,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        eventClick: $scope.alertOnEventClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender
      }
    };

    $scope.changeLang = function() {
      if($scope.changeTo === 'Hungarian'){
        $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
        $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
        $scope.changeTo= 'English';
      } else {
        $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        $scope.changeTo = 'Hungarian';
      }
    };
    
    /* event sources array*/
    $scope.eventSources = [$scope.events];
    $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];
    
}
]);

function CreateSessionCtrl($scope,$http,$location, $timeout,$modal,$modalInstance, teacherservice)
{
    $scope.session = {};
    $scope.session.user_id = teacherservice.teacherObj;
    $scope.instrumentList  = homeNoteAppConfig.instrumentList;
    $scope.skillLevelList  = homeNoteAppConfig.skillLevelList;

    $scope.create = function () 
    {
        console.log ($scope.session);

        $scope.session.start_date = moment($scope.session.start_date);
        $scope.session.end_date = moment($scope.session.end_date);

         $http({method: 'POST', url: '/api/session/sessionDetails', data:$scope.session}).
                            success(function(data, status, headers, config) {
                                    console.log (status);
                                 }).
                                    error(function(data, status, headers, config) 
                                    {
                                          noty({text: "Please validate the form !",  timeout: 2000, type: 'error'});
                                    });
    }
    $scope.cancel = function () 
    {
        $modalInstance.dismiss('cancel');
    };
        
}


homenoteapp.controller('SelectTeacherCtrl', ['$scope', '$http','$location', '$stateParams','$sce','localStorageService', 'teacherservice',
        function ($scope, $http,$location,$stateParams,$sce,localStorageService, teacherservice) 
{    
    $scope.filter = {};
    $scope.instrumentList  = homeNoteAppConfig.instrumentList;
    $scope.skillLevelList  = homeNoteAppConfig.skillLevelList;
    $scope.teacherList = [];

    $scope.submit = function () 
    {
        console.log ($scope.filter);
        $http({method: 'POST', url: '/api/session/getAllTeacherSessions', data:$scope.filter}).
        success(function(data, status, headers, config) {
                console.log (status);
                console.log (data);
                $scope.teacherList = data;

        }).
        error(function(data, status, headers, config) 
        {
              noty({text: "Please validate the form !",  timeout: 2000, type: 'error'});
        });
    }

    }
]);