homenoteapp.controller('TeacherPracticeSetCtrl', ['$scope', '$http',
        function ($scope, $http) {
            $scope.addpracticeset = {};
            $scope.addPracticeSet = function(){
                $http({method: 'POST', url: '/api/practiceset', data:{prcticeset:$scope.addpracticeset}}).
                            success(function(data, status, headers, config) {
                                noty({text: "AH",  timeout: 2000, type: 'success'});

                                /*$scope.username = null;
                                $scope.password = null;
                                $scope.check_password = null;*/
                            }).
                            error(function(data, status, headers, config) {
                                noty({text: data.message,  timeout: 2000, type: 'error'});
                            });
            };
            $scope.signup = function()
            {
                var salt = $scope.username;

                /*var enc_password = CryptoJS.PBKDF2($scope.password, salt, { keySize: 256/32 });
                var enc_check_password = CryptoJS.PBKDF2($scope.check_password, salt, { keySize: 256/32 });*/

                var user = {"username": $scope.username, "password": enc_password.toString(), "check_password" : enc_check_password.toString() };

                if($scope.username!==undefined || $scope.password !==undefined || $scope.check_password !==undefined){

                    if($scope.password !== $scope.check_password){
                        noty({text: 'password and check_password must be the same!',  timeout: 2000, type: 'warning'});
                    }else{
                        $http({method: 'POST', url: '/api/signup', data:user}).
                            success(function(data, status, headers, config) {
                                noty({text: "Username is registered correctly!",  timeout: 2000, type: 'success'});
                                $scope.username = null;
                                $scope.password = null;
                                $scope.check_password = null;
                            }).
                            error(function(data, status, headers, config) {
                                noty({text: data.message,  timeout: 2000, type: 'error'});
                            });
                    }

                }else{
                    noty({text: 'Username and password are mandatory!',  timeout: 2000, type: 'warning'});
                }

            }

        }
    ]);