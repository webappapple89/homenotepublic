angular.module("homenotedirectives",[])
.directive("chosen",function(){
	var linker = function(scope,element,attr){
		scope.$watch(function(){
			element.trigger("chosen:updated");
		});
		element.chosen({allow_single_deselect: true});
	};
	return {
		 priority: 9999,
		link:linker,
		scope:{}
	}
})
.directive('colorPicker', function() {
	  return {
	    scope: {
	      color: '=colorPicker'
	    },
	    link: function(scope, element, attrs) {
	      element.colorPicker({
	        // initialize the color to the color on the scope
	        pickerDefault: scope.color,
	        // update the scope whenever we pick a new color
	        onColorChange: function(id, newValue) {
	          scope.$apply(function() {
	            scope.color = newValue;
	          });
	        }
	      });

	      // update the color picker whenever the value on the scope changes
	      scope.$watch('color', function(value) {
	        element.val(value);
	        element.change();                
	      });
	    }
	  }
	})
.directive('ckeditor', [function () {
    return {
        require: '?ngModel',
        scope:{},
        link: function ($scope, elm, attr, ngModel) {
            var ck = CKEDITOR.replace(attr.id,{
            	toolbarGroups : [
            	                    	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            	                    	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            	                    	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
            	                    	{ name: 'forms' },
            	                    	'/',
            	                    	{ name: 'links' },
            	                    	{ name: 'insert' },
            	                    	{ name: 'styles' },
            	                    	{ name: 'colors' },
            	                    	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            	                    	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
            	                    ]
            	        });
            ck.on('pasteState', function () {
                $scope.$apply(function () {
                    ngModel.$setViewValue(ck.getData());
                });
            });
            ngModel.$render = function (value) {
                ck.setData(ngModel.$modelValue);
            };
        }
    };
}])
.directive('noteEditor', [function () {
    return {
        require: '?ngModel',
        scope:{},
        link: function ($scope, elm, attr, ngModel) {
            var ck = CKEDITOR.replace(attr.id,{
            	toolbarGroups : []
            	        });
            ck.on('pasteState', function () {
                $scope.$apply(function () {
                    ngModel.$setViewValue(ck.getData());
                });
            });
            ngModel.$render = function (value) {
                ck.setData(ngModel.$modelValue);
            };
        }
    };
}])
.directive('datePicker', function() {
    var linker = function(scope, element, attrs) {
    	$( ".from" ).datepicker({
    		  dateFormat: 'yy-mm-dd',
                      timeFormat: 'hh:mm tt',
    	     defaultDate: "+1w",
             changeMonth: true,
            changeYear: true,
            changeDay: true,
    	      onClose: function( selectedDate ) {
    	        $( ".to" ).datepicker( "option", "minDate", selectedDate );
    	      }
    	    });
    	    $( ".to" ).datepicker({
    	      dateFormat: 'yy-mm-dd',
              timeFormat: 'hh:mm tt',
    	     defaultDate: "+1w",
             changeMonth: true,
            changeYear: true,
            changeDay: true,
    	      onClose: function( selectedDate ) {
    	        $( ".from" ).datepicker( "option", "maxDate", selectedDate );
    	      }
    	    });
        element.datepicker({
            showOtherMonths: true,
          });
    };

    return {
        restrict: 'A',
        link: linker,
        scope:{}
    }
})
.directive('mogoSortable', function() {
	   return {
	      restrict: 'A',
	      scope:{},
	      link: function(scope, element, attrs) {
	        if (scope.$last=== true) {
	          element.ready(function () {
	              // Initialize UI Sortable
	              element.sortable();
	           });
	         }
	      }
	   };
	})
.directive('stopPropagation', function() {
    return {
        restrict: 'A',
        priority: 9999,
        link: function(scope, elem, attrs) {
        	angular.element(elem).bind('click', function(event){
        		event.stopPropagation()
        	})
        }
   };
})
.directive('scrollLoadmore', function() {
	return {
        restrict: 'A',
        priority: 999,
        scope:'&',
        link: function(scope, element, attrs) {
            angular.element(element).bind('scroll', function() {
                if (this.scrollTop + element[0].offsetHeight >= element[0].scrollHeight-70) {
               	 scope.loadMore();
                }
                if (this.scrollTop >= 10) {
                    $('div.static_display').addClass('static_bind');
                } else {
               	 $('div.static_display').removeClass('static_bind');
                }
           });
		}
	};
})
.directive('multipleEmails', function () {
    var EMAIL_REGEXP = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;

    function validateAll(ctrl, validatorName, value) {
        var validity = ctrl.$isEmpty(value) || value.split(',').every(
            function (email) {
                return EMAIL_REGEXP.test(email.trim());
            }
        );

        ctrl.$setValidity(validatorName, validity);
        return validity ? value : undefined;
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {
            function multipleEmailsValidator(value) {
                return validateAll(modelCtrl, 'multipleEmails', value);
            };

            modelCtrl.$formatters.push(multipleEmailsValidator);
            modelCtrl.$parsers.push(multipleEmailsValidator);
        } 
    };
})
.directive("loadMore",function(){
	var linker = function(scope,element,attr){
		scope.$watch(function(){
			element.Loadingdotdotdot();
		});
		element.Loadingdotdotdot();
	};
	return{
		restrict: 'A',
		link: linker,
		scope: {}
	}
})
.directive("phonePattern", function() {
    return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, el, attrs, ngModel) {
            var validator, patternValidator,
                pattern = attrs.phonePattern,
                required = true;
            
            if( pattern ) {
                if (pattern.match(/^\/(.*)\/$/)) {
                    pattern = new RegExp(pattern.substr(1, pattern.length - 2));
                    patternValidator = function(value) {
                        return validate(pattern, value)
                    };
                }
                else {
                    patternValidator = function(value) {
                        var patternObj = scope.$eval(pattern);
                        if (!patternObj || !patternObj.test) {
                            throw new Error('Expected ' + pattern + ' to be a RegExp but was ' + patternObj);
                        }
                        return validate(patternObj, value);
                    };
                }
            }
            
            ngModel.$formatters.push(patternValidator);
            ngModel.$parsers.push(patternValidator);
            
            attrs.$observe("required", function(newval) {
                required = newval;
                patternValidator(ngModel.$viewValue);
            });
            
            function validate(regexp, value) {
                if( value == null || value === "" || !required || regexp.test(value) ) {
                    ngModel.$setValidity('pattern', true);
                    return value;
                }
                else {
                    ngModel.$setValidity('pattern', false);
                    return;
                }
            }
        }
    };
}).directive('dateTimePicker', function() {
    var linker = function(scope, element, attrs) {
        jQuery( "#from1" ).datetimepicker({
                dateFormat: 'yy-mm-dd',
              timeFormat: 'HH:mm ',
              changeMonth: true,
            changeYear: true,
            changeDay: true,
                onClose: function(dateText, inst) {
                    if ($('#to1').val() != '') {
                        var testStartDate = $('#from1').datetimepicker('getDate');
                        var testEndDate = $('#to1').datetimepicker('getDate');
                        if (testStartDate > testEndDate)
                            $('#to1').datetimepicker('setDate', testStartDate);
                    }
                    else {
                        $('#to1').val(dateText);
                    }
                },
                onSelect: function (selectedDateTime){
                    $('#to1').datetimepicker('option', 'minDate', $('#from1').datetimepicker('getDate') );
                }
            });
        jQuery( "#to1" ).datetimepicker({
            dateFormat: 'yy-mm-dd',
          timeFormat: 'HH:mm ',
          changeMonth: true,
            changeYear: true,
            changeDay: true,
            onClose: function(dateText, inst) {
                if ($('#from1').val() != '') {
                    var testStartDate = $('#from1').datetimepicker('getDate');
                    var testEndDate = $('#to1').datetimepicker('getDate');
                    if (testStartDate > testEndDate)
                        $('#from1').datetimepicker('setDate', testEndDate);
                }
                else {
                    $('#from1').val(dateText);
                }
            },
            onSelect: function (selectedDateTime){
                $('#from1').datetimepicker('option', 'maxDate', $('#to1').datetimepicker('getDate') );
            }
        });
    };

    return {
        restrict: 'A',
        link: linker,
        scope:{}
    }
})
.directive('blur', function () {
  return function (scope, element, attrs) {
    attrs.$observe('blur', function (newValue) {
      newValue != "" && element[0].blur();
    });
  }
}).directive('multipleEmails', function () {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;

        function validateAll(ctrl, validatorName, value) {
            var validity = ctrl.$isEmpty(value) || value.split(',').every(
                function (email) {
                    return EMAIL_REGEXP.test(email.trim());
                }
            );

            ctrl.$setValidity(validatorName, validity);
            return validity ? value : undefined;
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function postLink(scope, elem, attrs, modelCtrl) {
                function multipleEmailsValidator(value) {
                    return validateAll(modelCtrl, 'multipleEmails', value);
                };

                modelCtrl.$formatters.push(multipleEmailsValidator);
                modelCtrl.$parsers.push(multipleEmailsValidator);
            } 
        };
    }).directive("compareTo",function(){
        return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    }
    }).directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
}).directive('compareTo', function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
}). directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
}).directive('optionsDisabled', function($parse) {
    var disableOptions = function(scope, attr, element, data, fnDisableIfTrue) {
        // refresh the disabled options in the select element.
        $("option[value!='?']", element).each(function(i, e) {
            var locals = {};
            locals[attr] = data[i];
            $(this).attr("disabled", fnDisableIfTrue(scope, locals));
        });
    };
    return {
        priority: 0,
        require: 'ngModel',
        link: function(scope, iElement, iAttrs, ctrl) {
            // parse expression and build array of disabled options
            var expElements = iAttrs.optionsDisabled.match(/^\s*(.+)\s+for\s+(.+)\s+in\s+(.+)?\s*/);
            var attrToWatch = expElements[3];
            var fnDisableIfTrue = $parse(expElements[1]);
            scope.$watch(attrToWatch, function(newValue, oldValue) {
                if(newValue)
                    disableOptions(scope, expElements[2], iElement, newValue, fnDisableIfTrue);
            }, true);
            // handle model updates properly
            scope.$watch(iAttrs.ngModel, function(newValue, oldValue) {
                var disOptions = $parse(attrToWatch)(scope);
                if(newValue)
                    disableOptions(scope, expElements[2], iElement, disOptions, fnDisableIfTrue);
            });
        }
    };
}).directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]).directive('showErrors', function() {
    return {
      restrict: 'A',
      require:  '^form',
      link: function (scope, el, attrs, formCtrl) {
        // find the text box element, which has the 'name' attribute
        var inputEl   = el[0].querySelector("[name]");
        // convert the native text box element to an angular element
        var inputNgEl = angular.element(inputEl);
        // get the name on the text box so we know the property to check
        // on the form controller
        var inputName = inputNgEl.attr('name');

        // only apply the has-error class after the user leaves the text box
        inputNgEl.bind('blur', function() {
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        })
      }
    }
  });
    

